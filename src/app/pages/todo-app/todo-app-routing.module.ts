import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TodoAppPage } from './todo-app.page';

const routes: Routes = [
  {
    path: '',
    component: TodoAppPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TodoAppRoutingModule {}
