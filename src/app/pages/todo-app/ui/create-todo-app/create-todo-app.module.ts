import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateTodoAppComponent } from './create-todo-app.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CreateTodoAppComponent],
  imports: [CommonModule, ReactiveFormsModule],
  exports: [CreateTodoAppComponent],
})
export class CreateTodoAppModule {}
