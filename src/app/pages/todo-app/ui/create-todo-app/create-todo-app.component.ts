import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-create-todo-app',
  templateUrl: './create-todo-app.component.html',
  styleUrls: ['./create-todo-app.component.scss'],
})
export class CreateTodoAppComponent implements OnInit {
  scheduleCreateFormGroup: FormGroup = new FormGroup({
    title: new FormControl(''),
    content: new FormControl(''),
  });

  constructor(private todoService: TodoService) {}

  ngOnInit(): void {}

  onCreate() {
    console.debug(this.scheduleCreateFormGroup.value);
    this.todoService.createSchedule({
      ...this.scheduleCreateFormGroup.value,
    });
    this.scheduleCreateFormGroup.reset();
  }
}
