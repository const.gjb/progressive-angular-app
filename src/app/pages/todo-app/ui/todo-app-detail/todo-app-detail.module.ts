import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoAppDetailComponent } from './todo-app-detail.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [TodoAppDetailComponent],
  imports: [CommonModule, ReactiveFormsModule],
  exports: [TodoAppDetailComponent],
})
export class TodoAppDetailModule {}
