import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoAppDetailComponent } from './todo-app-detail.component';

describe('TodoAppDetailComponent', () => {
  let component: TodoAppDetailComponent;
  let fixture: ComponentFixture<TodoAppDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodoAppDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TodoAppDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
