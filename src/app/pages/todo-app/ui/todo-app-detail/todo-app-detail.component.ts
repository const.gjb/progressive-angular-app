import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TodoService } from 'src/app/services/todo.service';
import { Schedule } from 'src/app/types/schedule.type';

@Component({
  selector: 'app-todo-app-detail',
  templateUrl: './todo-app-detail.component.html',
  styleUrls: ['./todo-app-detail.component.scss'],
})
export class TodoAppDetailComponent implements OnInit {
  @Input() schedule: Schedule = <Schedule>{
    title: '',
    content: '',
    id: 0,
  };

  scheduleUpdateFormGroup: FormGroup = new FormGroup({
    title: new FormControl(''),
    content: new FormControl(''),
    id: new FormControl(),
  });

  constructor(public todoService: TodoService) {}

  ngOnInit(): void {}

  ngOnChanges() {
    console.debug('hello');
    this.onSetDetail();
  }

  onUpdate() {
    console.debug(this.scheduleUpdateFormGroup.value);
    this.todoService.updateSchedule(this.scheduleUpdateFormGroup.value);
  }

  onSetDetail() {
    this.scheduleUpdateFormGroup.setValue(this.schedule);
  }
}
