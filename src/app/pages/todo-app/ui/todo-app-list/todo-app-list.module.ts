import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoAppListComponent } from './todo-app-list.component';

@NgModule({
  declarations: [TodoAppListComponent],
  imports: [CommonModule],
  exports: [TodoAppListComponent],
})
export class TodoAppListModule {}
