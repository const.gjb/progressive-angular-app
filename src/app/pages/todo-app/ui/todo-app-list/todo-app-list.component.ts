import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { TodoService } from 'src/app/services/todo.service';
import { Schedule } from 'src/app/types/schedule.type';

@Component({
  selector: 'app-todo-app-list',
  templateUrl: './todo-app-list.component.html',
  styleUrls: ['./todo-app-list.component.scss'],
})
export class TodoAppListComponent implements OnInit {
  schedules$: Observable<Schedule[]> = this.todoService.getSchedules();
  @Output() updateClick: EventEmitter<Schedule> = new EventEmitter();

  constructor(public todoService: TodoService) {}

  ngOnInit(): void {}

  onDelete(id: number) {
    this.todoService.deleteSchedule(id);
  }
}
