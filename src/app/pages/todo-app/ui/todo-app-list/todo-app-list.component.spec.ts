import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoAppListComponent } from './todo-app-list.component';

describe('TodoAppListComponent', () => {
  let component: TodoAppListComponent;
  let fixture: ComponentFixture<TodoAppListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodoAppListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TodoAppListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
