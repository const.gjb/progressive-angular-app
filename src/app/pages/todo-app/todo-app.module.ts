import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CreateTodoAppModule } from './ui/create-todo-app/create-todo-app.module';
import { TodoAppListModule } from './ui/todo-app-list/todo-app-list.module';
import { TodoAppDetailModule } from './ui/todo-app-detail/todo-app-detail.module';

import { TodoAppPage } from './todo-app.page';
import { TodoAppRoutingModule } from './todo-app-routing.module';

@NgModule({
  declarations: [TodoAppPage],
  imports: [
    CommonModule,
    TodoAppRoutingModule,
    ReactiveFormsModule,
    CreateTodoAppModule,
    TodoAppListModule,
    TodoAppDetailModule,
  ],
})
export class TodoAppModule {}
