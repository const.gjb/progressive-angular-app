import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';

import { TodoService } from 'src/app/services/todo.service';
import { Schedule } from 'src/app/types/schedule.type';

@Component({
  selector: 'app-todo-app',
  templateUrl: './todo-app.page.html',
  styleUrls: ['./todo-app.page.scss'],
})
export class TodoAppPage implements OnInit {
  schedule: Schedule = <Schedule>{
    title: '',
    content: '',
    id: 0,
  };

  constructor(public todoService: TodoService) {}

  ngOnInit(): void {}

  onUpdateClick(s: Schedule) {
    console.debug(s);
    this.schedule = s;
  }
}
