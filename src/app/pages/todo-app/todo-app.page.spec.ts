import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoAppPage } from './todo-app.page';

describe('TodoAppPage', () => {
  let component: TodoAppPage;
  let fixture: ComponentFixture<TodoAppPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodoAppPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TodoAppPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
