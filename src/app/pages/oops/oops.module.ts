import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OopsRoutingModule } from './oops-routing.module';
import { OopsPage } from './oops.page';

@NgModule({
  declarations: [OopsPage],
  imports: [CommonModule, OopsRoutingModule],
})
export class OopsModule {}
