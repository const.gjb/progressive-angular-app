import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OopsPage } from './oops.page';

const routes: Routes = [
  {
    path: '',
    component: OopsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OopsRoutingModule {}
