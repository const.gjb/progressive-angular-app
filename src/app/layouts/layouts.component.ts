import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs';
import { LayoutType } from './layout.type';

@Component({
  selector: 'app-layouts',
  templateUrl: './layouts.component.html',
  styleUrls: ['./layouts.component.scss'],
})
export class LayoutsComponent implements OnInit {
  layout: LayoutType = 'emtpy';

  constructor(private router: Router, private activatedRouter: ActivatedRoute) {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe(() => {
        this.layout = activatedRouter.snapshot.data['layout'] ?? 'emtpy';
      });
  }

  ngOnInit(): void {}
}
