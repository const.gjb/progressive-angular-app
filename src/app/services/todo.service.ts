import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {
  BehaviorSubject,
  concatAll,
  concatMap,
  filter,
  mergeMap,
  of,
  switchMap,
  tap,
} from 'rxjs';
import { environment } from 'src/environments/environment';
import { Schedule } from '../types/schedule.type';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  private readonly baseUrl: string = `${environment.server}/api/v1/schedules`;
  schedules$: BehaviorSubject<Schedule[]> = new BehaviorSubject<Schedule[]>([]);

  constructor(private httpClient: HttpClient) {}

  getSchedules() {
    return this.httpClient.get<Schedule[]>(`${this.baseUrl}`).pipe(
      switchMap((res) => {
        this.schedules$.next(res);
        return this.schedules$.asObservable();
      })
    );
  }

  getSchedule(id: number) {
    return this.schedules$.value.filter((s) => s.id === id)[0];
  }

  createSchedule(schedule: Schedule) {
    return this.httpClient
      .post<Schedule>(`${this.baseUrl}`, schedule)
      .subscribe((res) =>
        this.schedules$.next([...this.schedules$.value, res])
      );
  }

  updateSchedule(updateSchedule: Schedule) {
    return this.httpClient
      .put<Schedule>(`${this.baseUrl}/${updateSchedule.id}`, updateSchedule)
      .subscribe((res) => {
        console.debug(res);
        console.debug(this.schedules$.value);
        const schedulePos = this.schedules$.value.findIndex(
          (_schdule) => _schdule.id === res.id
        );
        console.debug(schedulePos);
        if (schedulePos === -1) {
          return;
        }
        console.debug(schedulePos);

        this.schedules$.value[schedulePos] = updateSchedule;
        this.schedules$.next(this.schedules$.value);
      });
  }

  deleteSchedule(id: number) {
    return this.httpClient
      .delete<Schedule>(`${this.baseUrl}/${id}`)
      .subscribe((res) => {
        console.debug(res);
        console.debug(this.schedules$.value);
        const deletedSchedules = this.schedules$.value.filter(
          (schedule) => schedule.id !== id
        );
        console.debug(deletedSchedules);
        this.schedules$.next(deletedSchedules);
      });
  }

  private getId(id: number) {
    if (id === 0) {
      const message = '일정을 찾을 수 없습니다.';
      window.alert(message);
      throw new Error(message);
    }
    return id;
  }
}
