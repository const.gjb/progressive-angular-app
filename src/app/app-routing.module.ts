import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutsComponent } from './layouts/layouts.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'todo-app',
  },
  {
    path: '',
    component: LayoutsComponent,
    data: {
      layout: 'emtpy',
    },
    children: [
      {
        path: 'oops',
        loadChildren: () =>
          import('./pages/oops/oops.module').then((m) => m.OopsModule),
      },
    ],
  },
  {
    path: '',
    component: LayoutsComponent,
    data: {
      layout: 'sidenav',
    },
    children: [
      {
        path: 'todo-app',
        loadChildren: () =>
          import('./pages/todo-app/todo-app.module').then(
            (m) => m.TodoAppModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
