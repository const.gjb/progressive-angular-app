export type Schedule = {
  id?: number;
  title: string;
  content: string;
};
